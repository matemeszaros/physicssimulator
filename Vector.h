#ifndef _VECTOR
#define _VECTOR

#include <math.h>
#include <iostream>

class Vector
{
public:
    // streaming operator
    friend std::ostream& operator<<(std::ostream& os, const Vector& v);

    // multiplication operator
    friend Vector operator*(double d, const Vector& v);

    // cross product operator - works onlyon 3 dimensional vectors
    friend Vector operator%(const Vector& u, const Vector& v);

    // Creates a Vector with a single 1 in the position specified as base.
    Vector(unsigned int dimension, int base = -1);

    // Creates a Vector from an array of numbers.
    Vector(unsigned int dimension, double* valueArray);

    // Copy constructor
    Vector(const Vector& v);

    // Destructor
    ~Vector();

    // gets the dimension of the Vector
    unsigned int getDimension() const;

    // Vector indexer (reference)
    double& operator[](unsigned int index);

    // Vector indexer (const)
    double operator[](unsigned int index) const;

    // assignment operator - changes the dimension if needed
	Vector& operator=(const Vector& v);

    // negate operator
    Vector operator-() const;

    // addition operator
    Vector operator+(const Vector& v) const;

    // substraction operator
    Vector operator-(const Vector& v) const;

    // dot product operator
    double operator*(const Vector& v) const;

    // multiplication operator
    Vector operator*(double d) const;

    // division operator
    Vector operator/(double d) const;

    // addition with asignment operator
    void operator+=(const Vector& v);

    // difference with asignment operator
    void operator-=(const Vector& v);

    // multiplication with asignment operator
    void operator*=(double d);

    // division with asignment operator
    void operator/=(double d);

    // concatenation operator
    Vector operator,(const Vector& v) const;

    // equality operator
    bool operator==(const Vector& v) const;

protected:
    // An array of the values.
    double* _values;
    // the dimension (length) of the Vector
    unsigned int _dimension;
private:
    // nothing yet!
};

// Calculates the absolute value of the vector
double abs(const Vector& v);

// Calculates a Vactor with same direction but unit length (absolute value).
Vector normalize(const Vector& v);

// finds the dimension (or size) of the Vector - deprecated by Vector.getDimension() method
int Dim(const Vector& v);

#endif // !_VECTOR
