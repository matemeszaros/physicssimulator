#include <math.h>
#include <stdlib.h>

#if defined(__APPLE__)                                                                                                                                                                                                            
#include <OpenGL/gl.h>                                                                                                                                                                                                            
#include <OpenGL/glu.h>                                                                                                                                                                                                           
#include <GLUT/glut.h>                                                                                                                                                                                                            
#else                                                                                                                                                                                                                             
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)                                                                                                                                                                       
#include <windows.h>                                                                                                                                                                                                              
#endif                                                                                                                                                                                                                            
#include <GL/gl.h>                                                                                                                                                                                                                
#include <GL/glu.h>                                                                                                                                                                                                               
#include <GL/glut.h>                                                                                                                                                                                                              
#endif          

#include "Vector.h"

const double Pi = 3.1415926535;
bool equal(double d1, double d2)
{
    return (abs(d1 - d2) < DBL_EPSILON);
}

//--------------------------------------------------------
// 3D Vector
//--------------------------------------------------------
struct Vector3
{
    double x, y, z;
    
    Vector3()
    {
        x = y = z = 0;
    }
    Vector3(double x0, double y0, double z0 = 0)
    {
        x = x0; y = y0; z = z0;
    }
    Vector3(const Vector& v)
    {
        if (Dim(v) == 3)
        {
            x = v[0];
            y = v[1];
            z = v[2];
        }
        else
        {
            throw "InvalidArgument";
        }
    }

    Vector3 operator-() const
    {
        return Vector3(-x, -y, -z);
	}
    Vector3 operator*(double a) const
    {
        return Vector3(x * a, y * a, z * a);
	}
    Vector3 operator/(double a) const
    {
        return Vector3(x / a, y / a, z / a);
    }
    Vector3 operator+(const Vector3& v) const
    {
        return Vector3(x + v.x, y + v.y, z + v.z);
    }
    Vector3 operator-(const Vector3& v) const
    {
        return Vector3(x - v.x, y - v.y, z - v.z);
    }
    void operator+=(const Vector3& v)
    {
        x += v.x;
        y += v.y;
        z += v.z;
    }
    void operator-=(const Vector3& v)
    {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }
    // dot product operator
    double operator*(const Vector3& v) const
    {
        return (x * v.x + y * v.y + z * v.z);
    }
    // cross product operator
    Vector3 operator%(const Vector3& v) const
    {
        return Vector3(y*v.z-z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
    }
    // equality operator
    bool operator==(const Vector3& v) const
    {
        return (abs(x-v.x) < DBL_EPSILON)
            && (abs(y-v.y) < DBL_EPSILON)
            && (abs(z-v.z) < DBL_EPSILON);
    }
    // paralell operator
    bool operator||(const Vector3& v) const
    {
        return (abs(x/v.x - y/v.y) < DBL_EPSILON)
            && (abs(y/v.y - z/v.z) < DBL_EPSILON)
            && (abs(z/v.z - x/v.x) < DBL_EPSILON);
        //return (normalize(*this) == normalize(v));
    }
    double Length() const
    {
        return sqrt(x * x + y * y + z * z);
    }
    operator Vector()
    {
        double valueArray[] = { x, y, z};
        return Vector(3,valueArray);
    }
};
Vector3 operator*(double d, const Vector3& v)
{
    return v*d;
}
Vector3 normalize(const Vector3& v)
{
	double L = v.Length();
    if (L != 0.0)
        return v/L;
    else
        return Vector3();
}
double abs(const Vector3& v)
{
	return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
}
Vector3 XX(1,0,0);
Vector3 YY(0,1,0);
Vector3 ZZ(0,0,1);
Vector3 GRAVITY(0,-9.81,0);

//--------------------------------------------------------
// Spektrum illetve szin
//--------------------------------------------------------
struct Color {
	double r, g, b;

	Color( ) {
		r = g = b = 0;
	}
	Color(double r0, double g0, double b0) {
		r = r0; g = g0; b = b0;
	}
	Color operator*(double a) {
		return Color(r * a, g * a, b * a);
	}
	Color operator*(const Color& c) {
		return Color(r * c.r, g * c.g, b * c.b);
	}
	Color operator+(const Color& c) {
		return Color(r + c.r, g + c.g, b + c.b);
	}
};

// Sajat strukturak

//--------------------------------------------------------
// Kvaternio
//--------------------------------------------------------
struct Quaternion {
	double r, x, y, z;
	Quaternion() {
		r = x = y = z = 0;
	}
	Quaternion(const Vector3& v) {
		r = 0; x = v.x; y = v.y; z = v.z;
	}
	Quaternion(double angleRad,const Vector3& axis) {
		Vector3 nAxis = normalize(axis);
		r = cosf(angleRad);
		x = sinf(angleRad)*nAxis.x;
		y = sinf(angleRad)*nAxis.y;
		z = sinf(angleRad)*nAxis.z;
	}
	Quaternion(double a, double b, double c, double d) {
		r = a; x = b; y = c; z = d;
	}
	Vector3 v() const {
        return Vector3( x, y, z);
	}
	Quaternion operator+(const Quaternion& q) const {
		return Quaternion(r + q.r, x + q.x, y + q.y, z + q.z);
	}
	Quaternion operator-(const Quaternion& q) const {
		return Quaternion(r - q.r, x - q.x, y - q.y, z - q.z);
	}
	Quaternion operator-() const {
		return Quaternion(-r, -x, -y, -z);
	}
	Quaternion operator~() const {
		return Quaternion(r, -x, -y, -z);
	}
	Quaternion operator*(const double f) const {
		return Quaternion(f*r, f*x, f*y, f*z);
	}
	Quaternion operator*(const Quaternion& q) const {
		double R = r*q.r - x*q.x - y*q.y - z*q.z;
		double X = r*q.x + x*q.r + y*q.z - z*q.y;
		double Y = r*q.y - x*q.z + y*q.r + z*q.x;
		double Z = r*q.z + x*q.y - y*q.x + z*q.r;
		return Quaternion(R, X, Y, Z);
	}
};
double abs(const Quaternion& q) {
	return sqrt(q.r*q.r + q.x*q.x + q.y*q.y + q.z*q.z);
}

//--------------------------------------------------------
// Catmull-Rom felulet
//--------------------------------------------------------
struct CRSurface {
	int		iSize;
	int		jSize;
	int uResolution;
	int vResolution;
	Vector3	**control;

	Color diffColor;
	Color specColor;
	double shininess;

	CRSurface() :iMax(100), jMax(100) {
		control = new Vector3*[iMax];
		for (int i = 0; i < iMax; ++i) {
			control[i] = new Vector3[jMax];
		}
		uResolution = 20;
		vResolution = 20;
	}

	void draw() {
		//glShadeModel(GL_FLAT);
		double dU = 1.0/uResolution;
		double dV = 1.0/vResolution;
		for (int i = 0; i < uResolution; ++i) {
			for (int j = 0; j < vResolution; ++j) {
				double u = i*dU;
				double v = j*dV;

				GLfloat glAmbColor[] = {diffColor.r/Pi, diffColor.g/Pi, diffColor.b/Pi, 1.0};
                glMaterialfv(GL_FRONT,GL_AMBIENT,glAmbColor);

				GLfloat glDiffColor[] = {diffColor.r, diffColor.g, diffColor.b, 1.0f};
				glMaterialfv(GL_FRONT,GL_DIFFUSE,glDiffColor);

				GLfloat glSpecColor[] = {specColor.r, specColor.g, specColor.b, 1.0f};
				glMaterialfv(GL_FRONT,GL_SPECULAR,glSpecColor);
				glMaterialf(GL_FRONT,GL_SHININESS,shininess);

				Vector3 position;
				Vector3 normal;
				glBegin(GL_QUADS);
				
				calcPosNorm(u, v, position, normal);
				glNormal3f(normal.x, normal.y, normal.z);
				glVertex3f(position.x, position.y, position.z);

				calcPosNorm(u, v + dV, position, normal);
				glNormal3f(normal.x, normal.y, normal.z);
				glVertex3f(position.x, position.y, position.z);

				calcPosNorm(u + dU, v + dV, position, normal);
				glNormal3f(normal.x, normal.y, normal.z);
				glVertex3f(position.x, position.y, position.z);

				calcPosNorm(u + dU, v, position, normal);
				glNormal3f(normal.x, normal.y, normal.z);
				glVertex3f(position.x, position.y, position.z);

				glEnd();
			}
		}
	}

private:
	int		iMax;
	int		jMax;

	void calcPosNorm(double u, double v, Vector3& position, Vector3& normal) {
		if (!(0 <= u && u <= 1 && 0 <= v && v <= 1))
			throw "InvalidArgument!";
		// Calculate the indexes (the bordering controlpoints are just for the continuity)
		double uBig = u*(iSize-3)+1;
		double vBig = v*(jSize-3)+1;
		int i = floorf(uBig);
		int j = floorf(vBig);
		double U = uBig - i;
		double V = vBig - j;
		
		Vector3 controlAtU[4];
		Vector3 controlAtV[4];
		for (int k = 0; k < 4; ++k) {
			controlAtU[k] = splinePoint(U,
				control[i-1][j-1+k],
				control[i+0][j-1+k],
				control[i+1][j-1+k],
				control[i+2][j-1+k]
			);
			controlAtV[k] = splinePoint(V,
				control[i-1+k][j-1],
				control[i-1+k][j+0],
				control[i-1+k][j+1],
				control[i-1+k][j+2]
			);
		}
		Vector3 Upoint;
		Vector3 Utangent;
		Vector3 Vpoint;
		Vector3 Vtangent;
		Upoint = splinePoint(V,
			controlAtU[0],
			controlAtU[1],
			controlAtU[2],
			controlAtU[3]
		);
		Vpoint = splinePoint(U,
			controlAtV[0],
			controlAtV[1],
			controlAtV[2],
			controlAtV[3]
		);
		Utangent = splineTangent(V,
			controlAtU[0],
			controlAtU[1],
			controlAtU[2],
			controlAtU[3]
		);
		Vtangent = splineTangent(U,
			controlAtV[0],
			controlAtV[1],
			controlAtV[2],
			controlAtV[3]
		);
		position = Upoint;
		normal = Utangent % Vtangent;
	}

	Vector3 splinePoint(double t, Vector3 P0, Vector3 P1, Vector3 P2, Vector3 P3) {
		Vector3 currPos = P1;
		Vector3 nextPos = P2;
		Vector3 currTan = (P2 - P0)*0.5;
		Vector3 nextTan = (P3 - P1)*0.5;
		Vector3 coeffs[4] = {
			currPos,
			currTan,
			(nextPos - currPos)*3 - (nextTan + currTan*2),
			(currPos - nextPos)*2 + (nextTan + currTan)
		};
		Vector3 pos;
		for (int i = 0; i < 4; ++i) {
			pos += coeffs[i]*powf(t,i);
		}
		return pos;
	}

	Vector3 splineTangent(double t, Vector3 P0, Vector3 P1, Vector3 P2, Vector3 P3) {
		Vector3 currPos = P1;
		Vector3 nextPos = P2;
		Vector3 currTan = (P2 - P0)*0.5;
		Vector3 nextTan = (P3 - P1)*0.5;
		Vector3 coeffs[4] = {
			currPos,
			currTan,
			(nextPos - currPos)*3 - (nextTan + currTan*2),
			(currPos - nextPos)*2 + (nextTan + currTan)
		};
		Vector3 tan;
		for (int i = 1; i < 4; ++i) {
			tan += coeffs[i]*powf(t,i-1)*i;
		}
		return tan;
	}
};

//--------------------------------------------------------
// Kamera
//--------------------------------------------------------
struct Camera {
private:
	Vector3 defaultEye;
	Vector3 defaultLookAt;
	Vector3 defaultUp;
public:
	double	ScreenWidth;
	double	ScreenHeight;

	Vector3	Eye;
	Vector3	LookAt;
	Vector3	Up;

	double	ViewAngle;
	double	NearCutPlane;
	double	FarCutPlane;

	Camera(Vector3 eye = Vector3(-10,6,8), Vector3 lookAt = Vector3(0,0,0), Vector3 up = Vector3(0,1,0)) {
		ScreenWidth = 1366;
        ScreenHeight = 768;
		
		defaultEye = Eye = eye;
		defaultLookAt = LookAt = lookAt;
		Vector3 direction = LookAt - Eye;
		defaultUp = Up = normalize((direction % up) % direction);

		ViewAngle = 45;
		NearCutPlane = 1;
		FarCutPlane = 200;
	}

	void moveForward(double units) {
		Vector3 forward = normalize(LookAt - Eye);
		Eye = Eye + forward*units;
		setOGL();
	}

	void moveRight(double units) {
		Vector3 right = normalize(LookAt - Eye) % Up;
		Eye = Eye + right*units;
		Vector3 forward = LookAt - Eye;
		Up = normalize((forward % Up) % forward);
		setOGL();
	}

	void moveUp(double units) {
		Eye = Eye + Up*units;
		Vector3 forward = LookAt - Eye;
		Up = normalize((forward % Up) % forward);
		setOGL();
	}

	void resetToInitial() {
		Eye = defaultEye;
		LookAt = defaultLookAt;
		Up = defaultUp;
		setOGL();
	}

	void setOGL() {
		glViewport(0, 0, ScreenWidth, ScreenHeight);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		gluPerspective(
			ViewAngle,
			ScreenWidth/ScreenHeight,
			NearCutPlane,
			FarCutPlane
			);
		
		gluLookAt(
			Eye.x,		Eye.y,		Eye.z,
			LookAt.x,	LookAt.y,	LookAt.z,
			Up.x,		Up.y,		Up.z
			);
	}
};

//--------------------------------------------------------
// Feny
//--------------------------------------------------------
struct Light {
	
	GLenum ID;
	
	GLfloat position[4];
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];

	Light(GLenum lightID, Vector3 point = Vector3(5,4,8), bool ideal = false) {
		ID = lightID;
		position[0] = point.x;
		position[1] = point.y;
		position[2] = point.z;
		position[3] = ideal ? 0.0 : 1.0;
	}

	void setPosition(Vector3 point, double weight = 1.0) {
		position[0] = point.x;
		position[1] = point.y;
		position[2] = point.z;
		position[3] = weight;
	}

	void setAmbient(Color a) {
		ambient[0] = a.r;
		ambient[1] = a.g;
		ambient[2] = a.b;
		ambient[3] = 1.0;
	}
	
	void setDiffuse(Color d) {
		diffuse[0] = d.r;
		diffuse[1] = d.g;
		diffuse[2] = d.b;
		diffuse[3] = 1.0;
	}

	void setSpecular(Color s) {
		specular[0] = s.r;
		specular[1] = s.g;
		specular[2] = s.b;
		specular[3] = 1.0;
	}

	void setOGL() {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glLightfv( ID, GL_AMBIENT, ambient);
		glLightfv( ID, GL_DIFFUSE, diffuse);
		glLightfv( ID, GL_SPECULAR, specular);
		glLightfv( ID, GL_POSITION, position);
		glEnable( ID);
	}
};

//--------------------------------------------------------
// Absztrakt Textura
//--------------------------------------------------------
struct Texture {
	virtual GLuint generate() = 0;
};

//--------------------------------------------------------
// Csempe Textura (Szobahoz)
//--------------------------------------------------------
struct TileTexture :public Texture{

	Color innerColor;
	Color outerColor;

	TileTexture(Color inner = Color(1,1,1), Color outer = Color(0,0,0)) {
		innerColor = inner;
		outerColor = outer;
	}

	virtual GLuint generate() {
		const int width = 16;
		const int height = 16;
        float *picture = new float[width * height * 3];

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				Color c = (0 < i && i < width-1) && (0 < j && j < height-1) ? innerColor : outerColor;
				picture[3*(i*height + j) + 0] = c.r;
				picture[3*(i*height + j) + 1] = c.g;
				picture[3*(i*height + j) + 2] = c.b;
			}
		}
		
		GLuint textureID;
        
		glEnable(GL_TEXTURE_2D);
        
		glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);

		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, picture);
        
		glDisable(GL_TEXTURE_2D);

		delete[](picture);

		return textureID;
	}
};

//--------------------------------------------------------
// Szem Textura (Fokahoz)
//--------------------------------------------------------
struct EyeTexture : public Texture {

	Color pupilColor;

	Color scleraColor;

	EyeTexture(Color pupil = Color(0,0,0), Color sclera = Color(1,1,1)) {
		pupilColor = pupil;
		scleraColor = sclera;
	}

	virtual GLuint generate() {
		const int width = 16;
		const int height = 16;
        float *picture = new float[width * height * 3];

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				Color c = (i < 3) ? pupilColor : scleraColor;
				picture[3*(i*height + j) + 0] = c.r;
				picture[3*(i*height + j) + 1] = c.g;
				picture[3*(i*height + j) + 2] = c.b;
			}
		}

		GLuint textureID;

        glEnable(GL_TEXTURE_2D);
        
		glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);

        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, picture);
        
		glDisable(GL_TEXTURE_2D);

		delete[](picture);

		return textureID;
	}
};

//--------------------------------------------------------
// Labda Textura
//--------------------------------------------------------
struct BallTexture :public Texture {

	Color firstColor;
	Color secondColor;

	BallTexture(Color first = Color(0,0,0), Color second = Color(1,1,1)) {
		firstColor = first;
		secondColor = second;
	}

	virtual GLuint generate() {
		const int width = 8;
		const int height = 8;
        float *picture = new float[width * height * 3];

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				Color c = (j % 2 == 0) ? firstColor : secondColor;
				picture[3*(i*height + j) + 0] = c.r;
				picture[3*(i*height + j) + 1] = c.g;
				picture[3*(i*height + j) + 2] = c.b;
			}
		}
		
		GLuint textureID;

        glEnable(GL_TEXTURE_2D);
        
		glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);

        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, picture);
        
		glDisable(GL_TEXTURE_2D);

		delete[](picture);

		return textureID;
	}
};

//--------------------------------------------------------
// Szoba
//--------------------------------------------------------
struct Room {
	Vector3 position;
	Vector3 rotationAxis;
	double rotationAngle;
	Vector3 scaling;

	bool isTextured;

	Color diffColor;
	Color specColor;
	double shininess;

	GLuint textureID;

	void initialize(Texture *texture) {
		isTextured = true;
		textureID = texture->generate();
		diffColor = Color(1,1,1);
		specColor = Color(1,1,1);
		shininess = 50;
	}

	void initialize(Color diff, Color spec, double shn) {
		isTextured = false;
		diffColor = diff;
		specColor = spec;
		shininess = shn;
	}

	void draw() {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(position.x, position.y, position.z);
		glRotatef(rotationAngle, rotationAxis.x, rotationAxis.y, rotationAxis.z);
		glScalef(scaling.x, scaling.y, scaling.z);

		
		GLfloat glAmbColor[] = {diffColor.r/Pi, diffColor.g/Pi, diffColor.b/Pi, 1.0};
		glMaterialfv(GL_FRONT,GL_AMBIENT,glAmbColor);

		GLfloat glDiffColor[] = {diffColor.r, diffColor.g, diffColor.b, 1.0f};
		glMaterialfv(GL_FRONT,GL_DIFFUSE,glDiffColor);

		GLfloat glSpecColor[] = {specColor.r, specColor.g, specColor.b, 1.0f};
		glMaterialfv(GL_FRONT,GL_SPECULAR,glSpecColor);
		glMaterialf(GL_FRONT,GL_SHININESS,shininess);
		
		if (isTextured) {
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, textureID);
		}

		int xTiles = scaling.x;
		int yTiles = scaling.y;
		int zTiles = scaling.z;

		glBegin(GL_QUADS);

		// floor
		glNormal3f( 0, 1, 0);
		glTexCoord2f( 0, 0);				glVertex3f(-1,-1,-1);
		glTexCoord2f( 0, zTiles);			glVertex3f(-1,-1, 1);
		glTexCoord2f( xTiles, zTiles);		glVertex3f( 1,-1, 1);
		glTexCoord2f( xTiles, 0);			glVertex3f( 1,-1,-1);

		// first-wall
		glNormal3f( 1, 0, 0);
		glTexCoord2f( 0, 0);				glVertex3f(-1, 1,-1);
		glTexCoord2f( 0, zTiles);			glVertex3f(-1, 1, 1);
		glTexCoord2f( yTiles, zTiles);		glVertex3f(-1,-1, 1);
		glTexCoord2f( yTiles, 0);			glVertex3f(-1,-1,-1);

		// second-wall
		glNormal3f( 0, 0, 1);
		glTexCoord2f( 0, 0);				glVertex3f( 1, 1,-1);
		glTexCoord2f( 0, xTiles);			glVertex3f(-1, 1,-1);
		glTexCoord2f( yTiles, xTiles);		glVertex3f(-1,-1,-1);
		glTexCoord2f( yTiles, 0);			glVertex3f( 1,-1,-1);

		// third-wall
		glNormal3f(-1, 0, 0);
		glTexCoord2f( 0, 0);				glVertex3f( 1, 1, 1);
		glTexCoord2f( 0, zTiles);			glVertex3f( 1, 1,-1);
		glTexCoord2f( yTiles, zTiles);		glVertex3f( 1,-1,-1);
		glTexCoord2f( yTiles, 0);			glVertex3f( 1,-1, 1);
		
		// fourth-wall
		glNormal3f( 0, 0,-1);
		glTexCoord2f( 0, 0);				glVertex3f(-1, 1, 1);
		glTexCoord2f( 0, xTiles);			glVertex3f( 1, 1, 1);
		glTexCoord2f( yTiles, xTiles);		glVertex3f( 1,-1, 1);
		glTexCoord2f( yTiles, 0);			glVertex3f(-1,-1, 1);
		
		// ceiling
		glNormal3f( 0,-1, 0);
		glTexCoord2f( 0, 0);				glVertex3f(-1, 1,-1);
		glTexCoord2f( 0, xTiles);			glVertex3f( 1, 1,-1);
		glTexCoord2f( zTiles, xTiles);		glVertex3f( 1, 1, 1);
		glTexCoord2f( zTiles, 0);			glVertex3f(-1, 1, 1);
		
		glEnd();

		glDisable(GL_TEXTURE_2D);
	}
};

//--------------------------------------------------------
// Labda
//--------------------------------------------------------
struct Sphere {
    double mass;
	Vector3	position;
	Vector3	velocity;



	Vector3	rotationAxis;
	double	rotationAngle;
	Vector3	scaling;

	int resolution;

	bool isTextured;

	Color diffColor;
	Color specColor;
	double shininess;

	GLuint textureID;

	Sphere() {
		scaling = Vector3(1,1,1);
		resolution = 30;
	}

	void initialize(Texture *texture) {
		isTextured = true;
		textureID = texture->generate();
		diffColor = Color(1,1,1);
		specColor = Color(1,1,1);
		shininess = 100;
	}

	void initialize(Color diff, Color spec, double shn) {
		isTextured = false;
		diffColor = diff;
		specColor = spec;
		shininess = shn;
	}

	double radius() const {
		return scaling.x;
	}

	void draw(bool concat = false) {
		glEnable(GL_NORMALIZE);
		glShadeModel(GL_SMOOTH);
		
		glMatrixMode(GL_MODELVIEW);
		if (!concat) {
			glLoadIdentity();
		}
		glTranslatef(position.x, position.y, position.z);
		glRotatef(rotationAngle, rotationAxis.x, rotationAxis.y, rotationAxis.z);
		glScalef(scaling.x, scaling.y, scaling.z);
		
		Vector3 top = Vector3(0,1,0);
		Vector3 bottom = Vector3(0,1,0);

		int horizontalResolution = 2*resolution;
		double dPhi = 2*Pi/horizontalResolution;
		int verticalResolution = resolution;
		double dTheta = Pi/verticalResolution;
		
		for (int i = 0; i < horizontalResolution; ++i) {
			
			double s1 = (i + 0.0)/horizontalResolution;
			double s2 = (i + 1.0)/horizontalResolution;

			double phi = i*dPhi;
			double phi2 = phi + dPhi;

			for (int j = 0; j < verticalResolution; ++j) {
				
				double t1 = (j + 0.0)/verticalResolution;
				double t2 = (j + 1.0)/verticalResolution;

				double theta = j*dTheta;
				double theta2 = theta + dTheta;
				
				double x, y, z;

				GLfloat glAmbColor[] = {diffColor.r/Pi, diffColor.g/Pi, diffColor.b/Pi, 1.0};
				glMaterialfv(GL_FRONT,GL_AMBIENT,glAmbColor);

				GLfloat glDiffColor[] = {diffColor.r, diffColor.g, diffColor.b, 1.0f};
				glMaterialfv(GL_FRONT,GL_DIFFUSE,glDiffColor);

				GLfloat glSpecColor[] = {specColor.r, specColor.g, specColor.b, 1.0f};
				glMaterialfv(GL_FRONT,GL_SPECULAR, glSpecColor);
				glMaterialf(GL_FRONT,GL_SHININESS, shininess);

				if (isTextured) {
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textureID);
				}

				glBegin(GL_QUADS);

				x = sinf(theta)*sinf(phi);
				y = cosf(theta);
				z = sinf(theta)*cosf(phi);
				glNormal3f(x, y, z);
				glTexCoord2f(s1,t1);
				glVertex3f(x, y, z);
				
				x = sinf(theta2)*sinf(phi);
				y = cosf(theta2);
				z = sinf(theta2)*cosf(phi);
				glNormal3f(x, y, z);
				glTexCoord2f(s1,t2);
				glVertex3f(x, y, z);
				
				x = sinf(theta2)*sinf(phi2);
				y = cosf(theta2);
				z = sinf(theta2)*cosf(phi2);
				glNormal3f(x, y, z);
				glTexCoord2f(s2,t2);
				glVertex3f(x, y, z);
				
				x = sinf(theta)*sinf(phi2);
				y = cosf(theta);
				z = sinf(theta)*cosf(phi2);
				glNormal3f(x, y, z);
				glTexCoord2f(s2,t1);
				glVertex3f(x, y, z);
				
				glEnd();

				glDisable(GL_TEXTURE_2D);

			}
		}
	}
};

//--------------------------------------------------------
// Interaktiv labda (gomb)
//--------------------------------------------------------
struct InteractiveSphere
{
private:
    Sphere _sphere;
public:
    double mass;
    Vector3& position;
	Vector3 velocity;
	Vector3 force;
    
    InteractiveSphere(Sphere sphere)
        :_sphere(sphere), position(_sphere.position)
    {
    }
    
    void initialize(Texture *texture)
    {
        _sphere.initialize(texture);
    }

    void initialize(Color diff, Color spec, double shn)
    {
        _sphere.initialize(diff, spec, shn);
    }

    void draw()
    {
        _sphere.draw();
    }
    
    void move(double sec)
    {
        Vector3 acceleration = force/mass;
        velocity += acceleration*sec;
        position += velocity*sec;
    }
    
    void collide(InteractiveSphere& other)
    {   
        double distance  = abs(other.position - position);
        
        if (distance < _sphere.radius() + other._sphere.radius())
        {
            double  m1 = mass;
            double  m2 = other.mass;
            Vector3 v1 = velocity;
            Vector3 v2 = other.velocity;

            Vector3 dirX = normalize(other.position - position);

            Vector3 v1x = (dirX*v1)*dirX;
            Vector3 v2x = (dirX*v2)*dirX;
            
            Vector3 v1p = v1 - v1x;
            Vector3 v2p = v2 - v2x;
            
            Vector3 w1x = (v1x*(m1 - m2) + v2x*(2*m2))/(m1 + m2);
            Vector3 w2x = (v2x*(m2 - m1) + v1x*(2*m1))/(m2 + m1);

            velocity = v1p + w1x;
            other.velocity = v2p + w2x;
        }
    }
    
    void collide(const Sphere& staticSphere)
    {
        Vector3 direction = position - staticSphere.position;
        bool moveTowards = (direction*velocity < 0);
        bool closeEnough = (abs(direction) < _sphere.radius() + staticSphere.radius());
        if (moveTowards && closeEnough)
        {
            Vector3 normal = normalize(position - staticSphere.position);
            // bounce off
            velocity = (velocity - normal*2*(normal*velocity));
        }
    }
    void collide(const Room& room)
    {
        Vector3 friction = normalize(velocity)*0.01;
        //velocity -= friction;

        double vabs = velocity.Length();
        if (vabs < 0)
        {
            velocity = Vector3();
        }
        // X
        if (_sphere.position.x - _sphere.scaling.x < room.position.x - room.scaling.x)
        {
            velocity.x = abs(velocity.x);
            velocity  = velocity*0.9;
        }
        if (_sphere.position.x + _sphere.scaling.x > room.position.x + room.scaling.x)
        {
            velocity.x = -abs(velocity.x);
            velocity  = velocity*0.9;
        }
        // Y
        if (_sphere.position.y - _sphere.scaling.y < room.position.y - room.scaling.y)
        {
            velocity.y = abs(velocity.y);
            velocity  = velocity*0.9;
        }
        if (_sphere.position.y + _sphere.scaling.y > room.position.y + room.scaling.y)
        {
            velocity.y = -abs(velocity.y);
            velocity  = velocity*0.9;
        }
        // Z
        if (_sphere.position.z - _sphere.scaling.z < room.position.z - room.scaling.z)
        {
            velocity.z = abs(velocity.z);
            velocity  = velocity*0.9;
        }
        if (_sphere.position.z + _sphere.scaling.z > room.position.z + room.scaling.z)
        {
            velocity.z = -abs(velocity.z);
            velocity  = velocity*0.9;
        }
    }
};

void addGravitationalPull(InteractiveSphere& s1, InteractiveSphere& s2)
{

}

//--------------------------------------------------------
// Csontozott Foka
//--------------------------------------------------------
struct Seal {
private:
	// A foka helyzete
	Vector3 position;
	double xRotation;

	CRSurface transformedSurface;
	CRSurface surface;

	Vector3 neckPoint;
	Vector3 neckRotationAngles;
	double neckAngle;

	Vector3 headPoint;
	Vector3 transformedHeadPoint;
	Vector3 headRotationAngles;
	double headAngle;
	
	Sphere nose;
	Vector3 nosePosition;
	Sphere rightEye;
	Vector3 rightEyePosition;
	Sphere leftEye;
	Vector3 leftEyePosition;

	Sphere headCollisionSphere;

	void initSurface() {
		
		surface.iSize = 11;
		surface.jSize = 7;
		surface.uResolution = 50;
		surface.vResolution = 30;

		// pre (head ring)
		surface.control[0][0] = Vector3(-0.8, 0.0, 4.3);
		surface.control[0][1] = Vector3( 0.0, 0.9, 4.3);
		surface.control[0][2] = Vector3( 0.8, 0.0, 4.3);
		surface.control[0][3] = Vector3( 0.0,-0.5, 4.3);
		surface.control[0][4] = Vector3(-0.8, 0.0, 4.3);
		surface.control[0][5] = Vector3( 0.0, 0.9, 4.3);
		surface.control[0][6] = Vector3( 0.8, 0.0, 4.3);
		//noses
		surface.control[1][0] = Vector3( 0.0, 0.2, 5.3);
		surface.control[1][1] = Vector3( 0.0, 0.2, 5.3);
		surface.control[1][2] = Vector3( 0.0, 0.2, 5.3);
		surface.control[1][3] = Vector3( 0.0, 0.2, 5.3);
		surface.control[1][4] = Vector3( 0.0, 0.2, 5.3);
		surface.control[1][5] = Vector3( 0.0, 0.2, 5.3);
		surface.control[1][6] = Vector3( 0.0, 0.2, 5.3);
		// head ring
		surface.control[2][0] = Vector3( 0.8, 0.0, 4.3);
		surface.control[2][1] = Vector3( 0.0,-0.5, 4.3);
		surface.control[2][2] = Vector3(-0.8, 0.0, 4.3);
		surface.control[2][3] = Vector3( 0.0, 0.9, 4.3);
		surface.control[2][4] = Vector3( 0.8, 0.0, 4.3);
		surface.control[2][5] = Vector3( 0.0,-0.5, 4.3);
		surface.control[2][6] = Vector3(-0.8, 0.0, 4.3);

		// head-neck ring
		surface.control[3][0] = Vector3( 0.8, 0.0, 3.6);
		surface.control[3][1] = Vector3( 0.0,-0.5, 3.6);
		surface.control[3][2] = Vector3(-0.8, 0.0, 3.6);
		surface.control[3][3] = Vector3( 0.0, 0.9, 3.6);
		surface.control[3][4] = Vector3( 0.8, 0.0, 3.6);
		surface.control[3][5] = Vector3( 0.0,-0.5, 3.6);
		surface.control[3][6] = Vector3(-0.8, 0.0, 3.6);
		// body-neck ring
		surface.control[4][0] = Vector3( 1.2, 0.0, 2.4);
		surface.control[4][1] = Vector3( 0.0,-0.6, 2.4);
		surface.control[4][2] = Vector3(-1.2, 0.0, 2.4);
		surface.control[4][3] = Vector3( 0.0, 1.2, 2.4);
		surface.control[4][4] = Vector3( 1.2, 0.0, 2.4);
		surface.control[4][5] = Vector3( 0.0,-0.6, 2.4);
		surface.control[4][6] = Vector3(-1.2, 0.0, 2.4);
		
		// body
		surface.control[5][0] = Vector3( 1.6, 0.0, 1.2);
		surface.control[5][1] = Vector3( 0.0,-0.7, 1.2);
		surface.control[5][2] = Vector3(-1.6, 0.0, 1.2);
		surface.control[5][3] = Vector3( 0.0, 1.7, 1.2);
		surface.control[5][4] = Vector3( 1.6, 0.0, 1.2);
		surface.control[5][5] = Vector3( 0.0,-0.7, 1.2);
		surface.control[5][6] = Vector3(-1.6, 0.0, 1.2);

		surface.control[6][0] = Vector3( 1.6, 0.0, 0.0);
		surface.control[6][1] = Vector3( 0.0,-0.7, 0.0);
		surface.control[6][2] = Vector3(-1.6, 0.0, 0.0);
		surface.control[6][3] = Vector3( 0.0, 1.6, 0.0);
		surface.control[6][4] = Vector3( 1.6, 0.0, 0.0);
		surface.control[6][5] = Vector3( 0.0,-0.7, 0.0);
		surface.control[6][6] = Vector3(-1.6, 0.0, 0.0);

		surface.control[7][0] = Vector3( 1.2, 0.0,-2.0);
		surface.control[7][1] = Vector3( 0.0,-0.6,-2.0);
		surface.control[7][2] = Vector3(-1.2, 0.0,-2.0);
		surface.control[7][3] = Vector3( 0.0, 1.0,-2.0);
		surface.control[7][4] = Vector3( 1.2, 0.0,-2.0);
		surface.control[7][5] = Vector3( 0.0,-0.6,-2.0);
		surface.control[7][6] = Vector3(-1.2, 0.0,-2.0);

		surface.control[8][0] = Vector3( 0.8, 0.0,-3.0);
		surface.control[8][1] = Vector3( 0.0,-0.5,-3.0);
		surface.control[8][2] = Vector3(-0.8, 0.0,-3.0);
		surface.control[8][3] = Vector3( 0.0, 0.6,-3.0);
		surface.control[8][4] = Vector3( 0.8, 0.0,-3.0);
		surface.control[8][5] = Vector3( 0.0,-0.5,-3.0);
		surface.control[8][6] = Vector3(-0.8, 0.0,-3.0);
		// tail
		surface.control[9][0] = Vector3( 0.0,-0.1,-3.6);
		surface.control[9][1] = Vector3( 0.0,-0.1,-3.6);
		surface.control[9][2] = Vector3( 0.0,-0.1,-3.6);
		surface.control[9][3] = Vector3( 0.0,-0.1,-3.6);
		surface.control[9][4] = Vector3( 0.0,-0.1,-3.6);
		surface.control[9][5] = Vector3( 0.0,-0.1,-3.6);
		surface.control[9][6] = Vector3( 0.0,-0.1,-3.6);
		// post
		surface.control[10][0] = Vector3(-0.8, 0.0,-3.0);
		surface.control[10][1] = Vector3( 0.0, 0.6,-3.0);
		surface.control[10][2] = Vector3( 0.8, 0.0,-3.0);
		surface.control[10][3] = Vector3( 0.0,-0.5,-3.0);
		surface.control[10][4] = Vector3(-0.8, 0.0,-3.0);
		surface.control[10][5] = Vector3( 0.0, 0.6,-3.0);
		surface.control[10][6] = Vector3( 0.8, 0.0,-3.0);
	}

	void initTransformedSurface() {
		transformedSurface.iSize = surface.iSize;
		transformedSurface.jSize = surface.jSize;

		for (int i = 0; i < 11; ++i) {
			for (int j = 0; j < 7; ++j) {
				transformedSurface.control[i][j] = surface.control[i][j];
			}
		}
		
		transformedSurface.uResolution = 50;
		transformedSurface.vResolution = 30;
	}

	Vector3 rotate(const Vector3& point, const Vector3& around, const Quaternion& rotation) const {
		return (rotation*(point - around)*~rotation).v() + around;
	}

	Quaternion calcRotation(const Vector3& rotationAngles) const {
		Vector3 halfRad = -rotationAngles*(Pi/180)*0.5;
		return Quaternion(halfRad.y, YY)*Quaternion(halfRad.x, XX)*Quaternion(halfRad.z, ZZ);
	}

public:

	Vector3 LookAtPoint;

	Seal() {
		initSurface();

		position = Vector3(0, 0, 0);
		xRotation = -5;

		neckPoint = Vector3(0,0.8,1.2);
		neckAngle = 5;
		headPoint = Vector3(0,0.6,3.9);
		headAngle = 2;
		
		nosePosition = Vector3( 0.0, 0.23, 5.25);
		nose.scaling = Vector3( 0.1, 0.1, 0.1);
		
		rightEyePosition = Vector3(-0.4, 0.5, 4.55);
		rightEye.scaling = Vector3( 0.15, 0.15, 0.15);

		leftEyePosition = Vector3( 0.4, 0.5, 4.55);
		leftEye.scaling = Vector3( 0.15, 0.15, 0.15);

		headCollisionSphere.position = Vector3(0,0.2,4.3);
		headCollisionSphere.scaling = Vector3(0.9,0.9,0.9);
	}

	void initTextures() {
		transformedSurface.diffColor = Color(0.1,0.1,0.1);
		transformedSurface.specColor = Color(1.5,1.5,1.5);
		transformedSurface.shininess = 20;

		EyeTexture eyeTexture = EyeTexture( Color(1,0,0), Color(1,1,1));
		rightEye.initialize(&eyeTexture);
		rightEye.resolution = 5;
		leftEye.initialize(&eyeTexture);
		leftEye.resolution = 5;

		nose.initialize(
			Color( 0.2, 0.1, 0.0),
			Color( 0.3, 0.2, 0.1),
			100);
		nose.resolution = 5;
	}

	void moveForward() {
		position.z += 0.5;
	}
	void moveBackward() {
		position.z -= 0.5;
	}

	void rotateNeckUp() {
		if (neckRotationAngles.x < 6*neckAngle)
			neckRotationAngles.x += neckAngle;
	}
	void rotateNeckDown() {
		if (-2*neckAngle < neckRotationAngles.x)
			neckRotationAngles.x -= neckAngle;
	}
	void rotateNeckLeft() {
		if (-4*neckAngle < neckRotationAngles.y)
			neckRotationAngles.y -= neckAngle;
	}
	void rotateNeckRight() {
		if (neckRotationAngles.y < 4*neckAngle)
			neckRotationAngles.y += neckAngle;
	}

	void rotateHeadUp() {
		if (headRotationAngles.x < 6*headAngle)
			headRotationAngles.x += headAngle;
	}
	void rotateHeadDown() {
		if (-2*headAngle < headRotationAngles.x)
			headRotationAngles.x -= headAngle;
	}
	void rotateHeadLeft() {
		if (-4*headAngle < headRotationAngles.y)
			headRotationAngles.y -= headAngle;
	}
	void rotateHeadRight() {
		if (headRotationAngles.y < 4*headAngle)
			headRotationAngles.y += headAngle;
	}

	void draw() {

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(position.x,position.y, position.z);

		// first initialize the transformed surface's controlpoints
		initTransformedSurface();
		// rotate the neck
		Quaternion neckRotation = calcRotation(neckRotationAngles);
		for (int i = 0; i < 5; ++i) {
			for (int j = 0; j < 7; ++j) {
				transformedSurface.control[i][j] = rotate(
					transformedSurface.control[i][j],
					neckPoint,
					neckRotation);
			}
		}
		// and the head point
		transformedHeadPoint = rotate(headPoint, neckPoint, neckRotation);
		// rotate the head 
		Quaternion headRotation = calcRotation(headRotationAngles);
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 7; ++j) {
				transformedSurface.control[i][j] = rotate(
					transformedSurface.control[i][j],
					transformedHeadPoint,
					headRotation);
			}
		}
		// and the last ring of the neck half that much
		Quaternion halfHeadRotation = calcRotation(headRotationAngles*0.5);
		for (int j = 0; j < 7; ++j) {
			transformedSurface.control[3][j] = rotate(
				transformedSurface.control[3][j],
				transformedHeadPoint,
				halfHeadRotation);
		}
		// rotate the nose
		nose.position = nosePosition;
		nose.position = rotate(nose.position, neckPoint, neckRotation);
		nose.position = rotate(nose.position, transformedHeadPoint, headRotation);
		// the right eye
		rightEye.position = rightEyePosition;
		rightEye.position = rotate(rightEyePosition, neckPoint, neckRotation);
		rightEye.position = rotate(rightEye.position, transformedHeadPoint, headRotation);
		// and the left eye
		leftEye.position = leftEyePosition;
		leftEye.position = rotate(leftEye.position, neckPoint, neckRotation);
		leftEye.position = rotate(leftEye.position, transformedHeadPoint, headRotation);

		// lookat :
		Vector3 lap = LookAtPoint - position;
		// right rotation
		Vector3 rightLap = normalize(lap - rightEye.position);
		rightEye.rotationAxis = YY%rightLap;
		rightEye.rotationAngle = acos(YY*rightLap)*(180/Pi);
		// left rotation
		Vector3 leftLap = normalize(lap - leftEye.position);
		leftEye.rotationAxis = YY%leftLap;
		leftEye.rotationAngle = acos(YY*leftLap)*(180/Pi);

		// finally draw it
		transformedSurface.draw();
		// nose
		glPushMatrix();
		nose.draw(true);
		glPopMatrix();
		// right eye
		glPushMatrix();
		rightEye.draw(true);
		glPopMatrix();
		// left eye
		glPushMatrix();
		leftEye.draw(true);
		glPopMatrix();
	}

	Sphere getHeadCollisionSphere() const {
		Sphere s;
		Quaternion neckRotation = calcRotation(neckRotationAngles);
		Quaternion headRotation = calcRotation(headRotationAngles);
		s.position = headCollisionSphere.position;
		s.position = rotate(s.position, neckPoint, neckRotation);
		s.position = rotate(s.position, headPoint, headRotation);
		s.position = s.position + position;
		s.scaling = headCollisionSphere.scaling;
		return s;
	}

};

Camera camera;

Light light0(GL_LIGHT0);

Seal seal;

Room room;

Sphere tempSphere;

InteractiveSphere iSphere1(tempSphere);
InteractiveSphere iSphere2(tempSphere);

void onInitialization( ) {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_CULL_FACE);

	camera.Eye = Vector3(-20, 10, 10);
	camera.Up = YY;
	camera.setOGL();

	light0.setPosition(Vector3(-0.0f, 10.0f, 0.0f), 0.5f);
	light0.setAmbient(Color(0.1f, 0.1f, 0.1f));
	light0.setDiffuse(Color(0.8f, 0.8f, 0.8f));
	light0.setSpecular(Color(1.2f, 1.2f, 1.2f));
	light0.setOGL();

	BallTexture ballTexture1 = BallTexture(Color(0.9, 0.9, 0.1), Color(0.9, 0.1, 0.2));
	BallTexture ballTexture2 = BallTexture(Color(0.1, 0.9, 0.1), Color(0.2, 0.1, 0.9));
	
	iSphere1.initialize(&ballTexture1);
	iSphere1.position = Vector3( 0, 16, 0);
	iSphere1.velocity = Vector3( 0, 0, 0);
    iSphere1.mass = 1;
    iSphere1.force = GRAVITY;

    iSphere2.initialize(&ballTexture2);
	iSphere2.position = Vector3( 0.5, 0, 0.2);
	iSphere2.velocity = Vector3( 0, 1, 0);
    iSphere2.mass = 1;
    iSphere2.force = GRAVITY;

	seal.initTextures();
	seal.LookAtPoint = Vector3(0,0,5);

	room.position = Vector3(0,9,0);
	room.scaling = Vector3(20,20,20);
	TileTexture tileTexture = TileTexture(Color(0.2, 0.5, 0.1), Color(0.1, 0.1, 0.2));
	room.initialize(&tileTexture);
}

void onDisplay( ) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	light0.setOGL();

	// ...
	room.draw();
	iSphere1.draw();
	iSphere2.draw();
	// ...

	glutSwapBuffers();
}

void onKeyboard(unsigned char key, int x, int y) {
	if (key == 'f') {
		camera.moveForward(0.5);
		glutPostRedisplay( );
	}
	if (key == 'b') {
		camera.moveForward(-0.5);
		glutPostRedisplay( );
	}
	if (key == 's') {
		seal.rotateNeckLeft();
		seal.rotateHeadLeft();
		glutPostRedisplay( );
	}
	if (key == 'd') {
		seal.rotateNeckRight();
		seal.rotateHeadRight();
		glutPostRedisplay( );
	}
	if (key == 'e') {
		seal.rotateNeckUp();
		seal.rotateHeadUp();
		glutPostRedisplay( );
	}
	if (key == 'x') {
		seal.rotateNeckDown();
		seal.rotateHeadDown();
		glutPostRedisplay( );
	}
	if (key == 'a') {
		seal.moveForward();
		glutPostRedisplay( );
	}
	if (key == 'y') {
		seal.moveBackward();
		glutPostRedisplay( );
	}


	if (key == '8') {
		camera.moveUp(0.5);
		glutPostRedisplay( );
	}
	if (key == '2') {
		camera.moveUp(-0.5);
		glutPostRedisplay( );
	}
	if (key == '6') {
		camera.moveRight(0.5);
		glutPostRedisplay( );
	}
	if (key == '4') {
		camera.moveRight(-0.5);
		glutPostRedisplay( );
	}
}

void onSpecial(int key, int x, int y)
{
}

void onMouse(int button, int state, int x, int y)
{

}

void onMotion(int x, int y)
{
    int modifiers = glutGetModifiers();
}



double elapsedMili = 0;

void onIdle( ) {
	long timeMili = glutGet(GLUT_ELAPSED_TIME);
	long dtmili = 1;
	while (elapsedMili <= timeMili) {
		double dtsec = dtmili/1000.0;
		iSphere1.collide(room);
		iSphere2.collide(room);
		iSphere1.collide(iSphere2);
		iSphere1.move(dtsec);
		iSphere2.move(dtsec);
		elapsedMili += dtmili;
	}
	elapsedMili = timeMili;

	glutPostRedisplay( );
}

// A C++ program belepesi pontja, a main fuggvenyt mar nem szabad bantani
int main(int argc, char **argv) {

	glutInit(&argc, argv); 				// GLUT inicializalasa
    glutInitWindowSize(camera.ScreenWidth, camera.ScreenHeight);			// Alkalmazas ablak kezdeti merete 600x600 pixel 
    glutInitWindowPosition(0, 0);			// Az elozo alkalmazas ablakhoz kepest hol tunik fel
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);	// 8 bites R,G,B,A + dupla buffer + melyseg buffer

    glutCreateWindow("Grafika hazi feladat");		// Alkalmazas ablak megszuletik es megjelenik a kepernyon

    glMatrixMode(GL_MODELVIEW);				// A MODELVIEW transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);			// A PROJECTION transzformaciot egysegmatrixra inicializaljuk
    glLoadIdentity();

    onInitialization();					// Az altalad irt inicializalast lefuttatjuk

    glutDisplayFunc(onDisplay);				// Esemenykezelok regisztralasa
    glutMouseFunc(onMouse); 
    glutMotionFunc(onMotion);
    glutIdleFunc(onIdle);
    glutKeyboardFunc(onKeyboard);
    glutSpecialFunc(onSpecial);

    glutMainLoop();					// Esemenykezelo hurok
    
    return 0;
}