#ifndef _VECTOR
#define _VECTOR

#include <math.h>
#include <iostream>

template <unsigned int N>
class VectorBase {
protected:
	// An array of the values.
	double values[N];
public:
	// Creates a VectorBase with a single 1 in the position specified as base.
	VectorBase(int base = -1) {
		for (int i = 0; i < N; ++i) {
			values[i] = (i == base) ? 1 : 0;
		}
	}
	// Creates a VectorBase from an array of numbers.
	VectorBase(double valueArray[N]) {
		for (int i = 0; i < N; ++i) {
			values[i] = valueArray[i];
		}
	}
	// Copy constructor
	VectorBase(const VectorBase& v) {
		for (int i = 0; i < N; ++i) {
			values[i] = v.values[i];
		}
	}
	// VectorBase indexer (reference)
	double& operator[](unsigned int index) {
		if (index < N)
			return values[index];
		throw "IndexOutOfBonds!";
	}
	// VectorBase indexer (const)
	double operator[](unsigned int index) const {
		if (index < N)
			return values[index];
		throw "IndexOutOfBonds!";
	}

	// maths:
	VectorBase operator=(const VectorBase& v) {
		for (int i = 0; i < N; ++i) {
			values[i] = v.values[i];
		}
		return *this;
	}
	VectorBase operator-() const {
		VectorBase negated;
		for (int i = 0; i <N; ++i) {
			negated.values[i] = -1*values[i];
		}
		return negated;
	}
	VectorBase operator+(const VectorBase& v) const {
		VectorBase sum;
		for (int i = 0; i < N; ++i) {
			sum.values[i] = values[i] + v.values[i];
		}
		return sum;
	}
	VectorBase operator-(const VectorBase& v) const {
		VectorBase difference;
		for (int i = 0; i < N; ++i) {
			difference.values[i] = values[i] - v.values[i];
		}
		return difference;
	}
	double operator*(const VectorBase& v) const {
		double dot = 0;
		for (int i = 0; i < N; ++i) {
			dot += values[i]*v.values[i];
		}
		return dot;
	}
	VectorBase operator*(double d) const {
		VectorBase product;
		for (int i = 0; i < N; ++i) {
			product.values[i] = values[i]*d;
		}
		return product;
	}
	VectorBase operator/(double d) const {
		VectorBase quotient;
		for (int i = 0; i < N; ++i) {
			quotient.values[i] = values[i]/d;
		}
		return quotient;
	}
	void operator+=(const VectorBase& v) {
		for (int i = 0; i < N) {
			values[i] += v.values[i];
		}
	}
	void operator-=(const VectorBase& v) {
		for (int i = 0; i < N) {
			values[i] -= v.values[i];
		}
	}
	void operator*=(double d) {
		for (int i = 0; i < N) {
			values[i] *= d;
		}
	}
	void operator/=(double d) {
		for (int i = 0; i < N) {
			values[i] /= d;
		}
	}
};

template<unsigned int N>
double abs(const VectorBase<N>& v) {
	return sqrt(v*v);
}

template<unsigned int N>
VectorBase<N> normalize(const VectorBase<N>& v) {
	double l = abs(v);
	return v/l;
}

template<unsigned int N>
int size(const VectorBase<N>& v) {
	return N;
}

template<unsigned int N>
std::ostream& operator<<(std::ostream& os, const VectorBase<N>& v) {
	os << "[";
	for (int i = 0; i < N-1; ++i) {
		os << v[i] << ";";
	}
	os << v[N-1] << "]";
	return os;
}

template<unsigned int N>
VectorBase<N> operator*(double d, const VectorBase<N>& v) {
	VectorBase<N> product;
	for (int i = 0; i < N; ++i) {
		product[i] = d*v[i];
	}
	return product;
}

VectorBase<3> operator%(const VectorBase<3>& u, const VectorBase<3>& v) {
	VectorBase<3> cross;
	cross[0] = u[1]*v[2] - u[2]*v[1];
	cross[1] = u[2]*v[0] - u[0]*v[2];
	cross[2] = u[0]*v[1] - u[1]*v[0];
	return cross;
}

template<unsigned int N>
class Vector : public VectorBase<N> {
public:
	Vector(int base = -1) :VectorBase(base) {}
	Vector(double valueArray[N]) :VectorBase(valueArray) {}
	Vector(const Vector& v) :VectorBase(v) {}
};

template<>
class Vector<3> : public VectorBase<3> {
public:
	double& x;
	double& y;
	double& z;
	Vector(double _x, double _y, double _z) :x(values[0]), y(values[1]), z(values[2]) {
		x = _x; y = _y; z = _z;
	}
	Vector(int base = -1) :VectorBase(base), x(values[0]), y(values[1]), z(values[2]) {}
	Vector(double valueArray[3]) :VectorBase(valueArray), x(values[0]), y(values[1]), z(values[2]) {}
	Vector(const VectorBase& v) :VectorBase(v), x(values[0]), y(values[1]), z(values[2]) {}
};

#endif // !_VECTOR
