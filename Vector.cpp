#include "Vector.h"

Vector::Vector(unsigned int dimension, int base)
    :_dimension(dimension)
{
    if (_dimension < 1)
        throw "InvalidArgument";

    _values = new double[_dimension];
    for (unsigned int i = 0; i < _dimension; ++i)
        _values[i] = (i == base) ? 1 : 0;
}

Vector::Vector(unsigned int dimension, double* valueArray)
    :_dimension(dimension)
{
    if (_dimension < 1)
        throw "InvalidArgument";
    
    _values = new double[_dimension];
    for (unsigned int i = 0; i < _dimension; ++i)
        _values[i] = valueArray[i];
}

Vector::Vector(const Vector& v)
    : _dimension(v._dimension)
{
    _values = new double[_dimension];
    for (unsigned int i = 0; i < _dimension; ++i)
        _values[i] = v._values[i];
}

Vector::~Vector()
{
    delete[](_values);
}

unsigned int
Vector::getDimension() const
{
    return _dimension;
}

double&
Vector::operator[](unsigned int index)
{
    if (index < _dimension)
        return _values[index];
    else
        throw "IndexOutOfBonds";
}

double
Vector::operator[](unsigned int index) const
{
    if (index < _dimension)
        return _values[index];
    else
        throw "IndexOutOfBonds";
}

Vector&
Vector::operator=(const Vector& v)
{
    if (_dimension != v._dimension)
    {
        delete[](_values);
        _dimension = v._dimension;
        _values = new double[_dimension];
    }
    for (unsigned int i = 0; i < _dimension; ++i)
        _values[i] = v._values[i];
    return *this;
}

Vector
Vector::operator-() const
{
    Vector negated(_dimension);
    for (unsigned int i = 0; i < negated._dimension; ++i)
        negated._values[i] = -1*_values[i];
    return negated;
}

Vector
Vector::operator+(const Vector& v) const
{
    if (_dimension != v._dimension)
        throw "DimensionInequality";
    
    Vector sum(_dimension);
    for (unsigned int i = 0; i < sum._dimension; ++i)
        sum._values[i] = _values[i] + v._values[i];
    return sum;
}

Vector
Vector::operator-(const Vector& v) const
{
    if (_dimension != v._dimension)
        throw "DimensionInequality";
    
    Vector difference(_dimension);
    for (unsigned int i = 0; i < difference._dimension; ++i)
        difference._values[i] = _values[i] - v._values[i];
    return difference;
}

double
Vector::operator*(const Vector& v) const
{
    if (_dimension != v._dimension)
        throw "DimensionInequality";
    
    double dot = 0;
    for (unsigned int i = 0; i < v._dimension; ++i)
        dot += _values[i]*v._values[i];
    return dot;
}

Vector
Vector::operator*(double d) const
{
    Vector product(_dimension);
    for (unsigned int i = 0; i < _dimension; ++i)
        product._values[i] = _values[i]*d;
    return product;
}

Vector
Vector::operator/(double d) const
{
    if (d == 0)
        throw "DivisionByZero";
    
    Vector quotient(_dimension);
    for (unsigned int i = 0; i < _dimension; ++i)
        quotient._values[i] = _values[i]/d;
    return quotient;
}

void
Vector::operator+=(const Vector& v)
{
    if (_dimension != v._dimension)
        throw "DimensionInequality";
    
    for (unsigned int i = 0; i < v._dimension; ++i)
        _values[i] += v._values[i];
}

void
Vector::operator-=(const Vector& v)
{
    if (_dimension != v._dimension)
        throw "DimensionInequality";
    
    for (unsigned int i = 0; i < v._dimension; ++i)
        _values[i] -= v._values[i];
}

void
Vector::operator*=(double d)
{
    for (unsigned int i = 0; i < _dimension; ++i)
        _values[i] *= d;
}

void
Vector::operator/=(double d)
{
    if (d == 0)
        throw "DivisionByZero";
    
    for (unsigned int i = 0; i < _dimension; ++i)
        _values[i] /= d;
}

Vector
Vector::operator,(const Vector& v) const
{
    Vector concatenation(_dimension + v._dimension);
    for (unsigned int i = 0; i < concatenation._dimension; ++i) {
        if (i < _dimension)
            concatenation._values[i] = _values[i];
        else
            concatenation._values[i] = v._values[i-_dimension];
    }
    return concatenation;
}

bool
Vector::operator==(const Vector& v) const
{
    Vector first(*this);
    Vector second(v);
    
    if (first._dimension < second._dimension)
    {
        unsigned int difference = second._dimension - first._dimension;
        first = first , Vector(difference);
    }
    else if (first._dimension > second._dimension)
    {
        unsigned int difference = first._dimension - second._dimension;
        second = second , Vector(difference);
    }

    bool isEqual = true;
    for (unsigned int i = 0; i < first._dimension; ++i)
        isEqual = isEqual && (first._values[i] == second._values[i]);
    
    return isEqual;
}

std::ostream&
operator<<(std::ostream& os, const Vector& v)
{
    os << "[";
    
    for (unsigned int i = 0; i < v._dimension-1; ++i)
        os << v._values[i] << ";";
    
    os << v._values[v._dimension-1] << "]";
    
    return os;
}

Vector
operator*(double d, const Vector& v)
{
    return v*d;
}

Vector
operator%(const Vector& u, const Vector& v)
{
    if (u.getDimension() != 3 || v.getDimension() != 3)
        throw "InvalidCall";
    
    Vector cross(3);
    cross[0] = u[1]*v[2] - u[2]*v[1];
    cross[1] = u[2]*v[0] - u[0]*v[2];
    cross[2] = u[0]*v[1] - u[1]*v[0];
    return cross;
}

double abs(const Vector& v)
{
    return sqrt(v*v);
}

Vector
normalize(const Vector& v)
{
    double length = abs(v);
    if (length == 0)
        throw "NullVector";
    return v/length;
}

int
Dim(const Vector& v)
{
    return v.getDimension();
}